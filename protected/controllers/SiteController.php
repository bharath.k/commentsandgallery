<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
    public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}
	public function actionregister()
	{
		$model=new User;
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$model1=User::model()->findByAttributes(array('username'=>$model->username));
			if(empty($model1)){
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}
		else{
			echo "username is already taken";
			exit();
		}
	}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	public function actionViewcomment()
	{
		$model=Comments::model()->findAll();
		$this->render('commentview',array('model'=>$model));
	}
		#$this->render('commentview',array('model'=>$this->loadModel($username));
		#$this->render('commentview',array('model'=>$username),);
		#$model=User::model()->findByAttributes(array('username'=>$this->username));
		
#	}
	//public function loadModelcomment($username){
	//$username=Yii::app()->user->name;
	//if($username==null)
	//	throw new CHttpException(404,'The requested page does not exist.');
//	return $username;
//	}
	/** 
	*{
	*	#$model=User::model()->findByAttributes(array('username'=>$this->username));
	*	$model=User::model()->findByAttributes($username);
	*	if($model===null)
	*		throw new CHttpException(404,'The requested page does not exist.');
	*	return $model;
	*}*/
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */

	
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect('index.php?r=site/comments');
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}
	public function actionComments()
{
	#die("hellos");
	  $model =new Comments;
	//    exit();
		if(isset($_POST['Comments']['comment']))
		{
			#print_r($_POST['Comments']['comment']);
			$username=Yii::app()->user->name;
			$model->comment=$_POST['Comments']['comment'];
			$model->username=$username;
			if($model->save()){
				$this->redirect('index.php?r=site/comments');
			#$this->redirect(array('viewcomment','username'=>$model->username));
		}
		}
		 //$model=new Comments;
		$this->render('comment',array(
			'model'=>$model,
		));
}	
public function actionUploadimage()
{
	$model=new Image;
	$username=Yii::app()->user->name;
   if(isset($_POST['submit'])){
      $errors= array();
      $file_name = $_FILES['image']['name'];
      $file_tmp = $_FILES['image']['tmp_name'];
      $file_type = $_FILES['image']['type'];
      $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
      
      $expensions= array("jpeg","jpg","png");
      
      if(in_array($file_ext,$expensions)=== false){
         $errors[]="extension not allowed, please choose a JPEG or PNG file.";
      }
      if(empty($errors)==true) {
         if(move_uploaded_file($file_tmp,"images/".$file_name)){
			 $model->imagename=$file_name;
			 $model->username=$username;
			 if($model->save()){
				$this->redirect('index.php?r=site/uploadimage');
			 }
		 }
      }else{
         print_r($errors);
      }
   }
	$this->render('uploadimage',array(
		'model'=>$model,
	));

}
public function actionUpdateimage(){
	$id=$_REQUEST['id'];
	$model=$this->imageloadModel($id);
   if(isset($_POST['submit'])){
      $errors= array();
      $file_name = $_FILES['image']['name'];
      $file_tmp = $_FILES['image']['tmp_name'];
      if(empty($errors)==true) {
         if(move_uploaded_file($file_tmp,"images/".$file_name)){
			 $model->imagename=$file_name;
			 if($model->save()){
				$this->redirect('index.php?r=site/uploadimage');
			 }
		 }
      }else{
         print_r($errors);
      }
   }
	$this->render('updateimage',array(
		'model'=>$model,
	));
}
public function actionViewimage()
{
	$model=Image::model()->findAll();
	$this->render('viewimage',array('model'=>$model));
}
public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	public function actionCommentupdate()
	{
		if(isset($_POST['Comments']['comment']))
		{
			$model=$this->commentloadModel($_POST['Comments']['commentid']);
			$model->comment=$_POST['Comments']['comment'];
			if($model->save())
			$this->redirect('index.php?r=site/comments');
		}
		$id=$_REQUEST['id'];
		$model=$this->commentloadModel($id);

		$this->render('commentupdate',array(
			'model'=>$model,
		));
	}

public function actionImagedelete()
	{
		$id=$_REQUEST['id'];
		$this->imageloadModel($id)->delete();
		$this->redirect('index.php?r=site/uploadimage');
	}
	public function imageloadModel($id)
	{
		$model=Image::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionCommentdelete()
	{
		$id=$_REQUEST['id'];
		$this->commentloadModel($id)->delete();
		$this->redirect('index.php?r=site/comments');
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		//if(!isset($_GET['ajax']))
			//$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	public function commentloadModel($id)
	{
		$model=Comments::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

}