<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
/*	<a href="delcomment.php?id=<?php echo $r['id']; ?>"*/
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'user-comment-form',
	'enableAjaxValidation'=>false,
)); ?>
<h1>Comment</h1>


<div class="form">


	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'comment'); ?>
		<?php echo $form->textArea($model,'comment'); ?>
		<?php echo $form->error($model,'comment'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<!DOCTYPE html>
<html>
<head>
	<title>Home Page</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >
	<link rel="stylesheet" href="styles.css" >
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
</head>
<body>
<?php
$model2=Comments::model()->findAll();
$username1=Yii::app()->user->name;
?>

<div class="container">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">Comments</div>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Comment id</th>
						<th>Name</th>
						<th>Comment</th>
						<th>update</th>
					</tr>
				</thead>
				<tbody>
				<?php
                foreach($model2 as $res ){
				?>
					<tr>
						<td><?php echo $res->commentid; ?></td>
						<td><?php echo $res->username; ?></td>
						<td><?php echo $res->comment; ?></td>
						<?php if($res->username==$username1)

					{	 ?>
						<td><a href="index.php?r=site/commentupdate&id=<?php echo $res->commentid?>">Edit</a>  <a href="index.php?r=site/commentdelete&id=<?php echo $res->commentid?>">Del</a></td>
					
<?php }?>
	

					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>

	</div>
</div>
</body>
</html>
