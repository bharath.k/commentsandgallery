
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'user-comment-form',
	'enableAjaxValidation'=>false,
)); ?>
<h1>Comment</h1>


<div class="form">


	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'comment'); ?>
		<?php echo $form->textArea($model,'comment'); ?>
		<?php echo $form->error($model,'comment'); ?>
        <?php echo $form->hiddenField($model,'commentid'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->