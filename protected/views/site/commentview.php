
 <!DOCTYPE html>
<html>
<head>
	<title>Home Page</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >
	<link rel="stylesheet" href="styles.css" >
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
</head>
<body>


<div class="container">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">Comments</div>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Comment id</th>
						<th>Name</th>
						<th>Comment</th>
					</tr>
				</thead>
				<tbody>
				<?php
                foreach($model as $res ){
				?>
					<tr>
						<td><?php echo $res->commentid; ?></td>
						<td><?php echo $res->username; ?></td>
						<td><?php echo $res->comment; ?></td>

	

					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>

	</div>
</div>
</body>
</html>
